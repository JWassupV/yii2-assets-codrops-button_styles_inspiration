<?

namespace jwassupv\themes\admin\adminlte2;


class ButtonStylesInspirationAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/adminlte';
    public $css = [
        'dist/css/AdminLTE.min.css',
        'dist/css/skins/_all-skins.min.css',
    ];
    public $js = [
        'dist/js/app.min.js',
        'plugins/slimScroll/jquery.slimscroll.min.js'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];

}